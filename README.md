# Roadmaps

Built with [Sphinx](https://www.sphinx-doc.org) using the [pydata-sphinx-theme](https://pydata-sphinx-theme.readthedocs.io/en/stable/index.html) and deployed to 
[Read the Docs](https://readthedocs.org/).

[![Documentation Status](https://readthedocs.org/projects/roadmaps/badge/?version=latest)](https://data.tymyrddin.dev/projects/roadmaps/en/latest/?badge=latest)

A generic implementation roadmap that can facilitate MLOps for any ML problem in detail. The goal of this roadmap is to solve the problem with the right solution.

## Contributing

This project welcomes contributions.

## License

Unlicensed. Or Universal Licensed. Whatever. This is in our playground.
