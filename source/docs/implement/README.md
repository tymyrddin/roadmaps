# Introduction

## What

Implementing roadmap.

## Why

Create a robust, scalable, frugal, and sustainable MLOps process.

## How?

* [ML development](dev.md)
* [Transition to operations](transition.md)
* [Operations](ops.md)