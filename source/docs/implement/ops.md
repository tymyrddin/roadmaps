# Operations

In this phase, the deployed model performance is monitored in terms of model drift, bias, and/or other metrics. Based on the model's performance, we can enable continual learning via periodic model retraining and enable alerts and actions. Simultaneously, we monitor logs in telemetry data for the production environment to detect any possible errors and resolve them on the go to ensure  uninterrupted working of the production system. We also manage data pipelines, the ML platform, and security on the go. 

With successful implementation of this phase, we can monitor the deployed models and retrain them in a robust, scalable, and secure manner.