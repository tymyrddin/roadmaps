# ML development

The genesis of implementing the MLOps framework for a problem; before beginning to implement the requirements, the problem and solution must be clear and vivid. 

Taken into account are the system requirements to design and implement a robust and scalable MLOps framework. We begin by selecting the right tools and infrastructure needed (storage, compute, and so on) to implement the MLOps.

When the infrastructure is set up, the necessary workspace and the development and test environments to execute ML experiments (training and testing) are available. We can train the ML models using the development environment and test the models for performance and functionality using test data in the development or test environments, depending on the workflow or requirement. When infrastructure is set up and the first ML model is trained, tested, serialised, and packaged, phase 1 of the MLOps framework is set up and validated for robustness. Serialising and containerising is an important process to standardise and get the models ready for deployments.