# Learning models

## Supervised learning

[Supervised learning models or algorithms](https://gitlab.com/tymyrddin/ml/-/tree/main/source/notebooks/supervised_algorithms) are trained based on labeled data. In the training data, the result of the input is marked or known. A model is trained to predict an outcome given an input based on the labeled data it learns from, and the system is "told" which output corresponds with a given input in the system.

Supervised learning models are very effective on narrow AI cases and well-defined tasks but can only be harnessed where there is sufficient and comprehensive labeled data.

## Unsupervised learning

[Unsupervised learning models or algorithms](https://gitlab.com/tymyrddin/ml/-/tree/main/source/notebooks/unsupervised_algorithms) learn from unlabeled data. Unsupervised learning can be used to mine insights and identify patterns from unlabeled data. Unsupervised algorithms are often used for clustering or anomaly detection without relying on any labels. These algorithms can be pattern-finding algorithms; when data is fed to such an algorithm, it will identify patterns and turn those into a recipe for taking a new data input without a label and applying the correct label to it.

Unsupervised learning is used mostly for analytics, and can also be used for automation and ML. It is recommended not to use these algorithms in production because of their dynamic nature. They can be useful to automate certain processes like mining patterns in data or identifying anomalies in real time.