# Statistical models

## Inductive learning 

Inductive learning is a statistical method that generalises from specific examples in the training data, using this evidence to determine the most likely outcome. It involves a process of learning by example, where a system tries to generalise a general function or rule from a set of observed instances. Fitting an ML model is a process of induction. 

## Deductive learning

Deductive learning refers to using general rules to determine specific outcomes. The outcomes of deductive learning are deterministic and specific, whereas for inductive reasoning, the conclusions are probabilistic or generalised. In a way, deduction is the reverse of induction. If induction goes from the specific to the general, deduction goes from the general to the specific.

## Transductive learning

Transductive learning is a method for reasoning about outcomes based on specific training data samples (in the training dataset). This method is different from inductive learning, where predictions are generalised over the training data. In transductive learning, specific or similar data samples from the training data are compared to reason about or predict an outcome. 

For example, in the case of the k-nearest neighbours algorithm, it uses specific data samples on which to base its outcome rather than generalising the outcome or modeling with the training data.