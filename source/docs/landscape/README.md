# Introduction

## What?

The landscape of ML models.

## Why?

Make an efficient algorithm selection.

## How?

* [Learning models](learning.md)
* [Hybrid models](hybrid.md)
* [Statistical models](statistical.md)
* [Human-In-The-Loop models](hitl.md)