# Human-In-The-Loop models

## Human-centered reinforcement learning

Human-centered reinforcement learning, also known as interactive reinforcement learning, is [a hybrid of reinforcement learning](https://arxiv.org/abs/2005.11016), involving humans in the loop to monitor the agent's learning and provide evaluative feedback to shape the learning of the agent.

Human reinforcement learning is highly efficient in environments where the agent has to learn or [mimic human behaviour](https://pubmed.ncbi.nlm.nih.gov/36435390/).

## Active learning

Active learning is a method where the trained model can query a human user during the inference process to resolve incertitude during the learning process. For example, this could be a question-answering chatbot asking the human user for validation by asking yes or no questions.