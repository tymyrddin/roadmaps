# Introduction

## What

Getting the factors needed for success.

## Why

Implementing successful MLOps depends on factors such as getting appropriate training data, having high standards, and fitting requirements, tools, and infrastructure.

## How?

* [Data](data.md)
* [Requirements](reqs.md)
