# Requirements

Requirements are to include important factors such as ethical, explainability and regulatory factors. 

* Discrimination or bias (such as who or what is predicted or classified) is critical for the application and which properties should be preserved as part of data privacy (for example, some properties should not be used for predictions or classification, such as race, age, or gender).
* Explainability requirements must explicitly be taken into account to explain situations and decisions of the ML solution or system to the users of the system. 
* The requirements must stipulate regulations and restrictions to adhere to concerning the use of the data and validation of decisions made by the ML system.