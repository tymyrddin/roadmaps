# Introduction

## What?

MLOps structure.

## Why?

Identify a suitable ML solution to a problem.

## How?

* [Small data ops](small.md)
* [Big data ops](big.md)
* [Hybrid MLOps](hybrid.md)
* [Large-scale MLOps](large.md)
