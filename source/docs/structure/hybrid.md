# Hybrid MLOps

Hybrid teams operate with experienced data scientists, data engineers, and DevOps engineers, and make use of ML capabilities to support their business operations. They are further ahead in implementing MLOps compared to other teams and work with big data and open source software tools such as PyTorch, TensorFlow, and scikit-learn, and hence have a requirement for efficient collaboration. They often work on well-defined problems by implementing robust and scalable software engineering practices. 

This team is still prone to challenges:

* Incurring huge costs, or more than expected, due to mundane and repeated work to be done by data scientists, such as repeating data cleaning or feature engineering.
* Inefficient model monitoring and retraining mechanisms.

These operations are typical for hybrid ops:

* The team consists of data scientists, data engineers, and DevOps engineers.
* High requirement for efficient and effective collaboration.
* High requirement for big data processing capacity.
* High support requirements for open source technologies such as PyTorch, TensorFlow, and scikit-learn for any kind of ML, from classical to deep learning, and from supervised to unsupervised learning.