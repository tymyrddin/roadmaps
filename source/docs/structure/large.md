# Large-scale MLOps

Large-scale operations are common in big companies with large or medium-sized engineering teams consisting of data scientists, data engineers, and DevOps engineers. They run operations on the scale of big data, or with various types of data on various scales, veracity, and velocity. Teams have multiple legacy systems to manage to support business operations. 

Such teams or organizations can be prone to:

* Incurring huge costs, or more than expected, due to mundane and repeated work.
* Code and data starting to grow independently.
* Having bureaucratic and highly regulated processes and quality checks.
* Highly entangled systems and processes – when one thing breaks, everything breaks.

Operations with a setup like this one can be considered large-scale ops:

* The team consists of data scientists, data engineers, and DevOps engineers.
* Large-scale inference and operations.
* Big data operations.
* ML model management on multiple resources.
* Big or multiple teams.
* Multiple use cases and models.

