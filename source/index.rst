Roadmaps
==============================================

A generic implementation roadmap that can facilitate MLOps for any ML problem in detail. This roadmap is intended for reducing problem solutioning, and using problem solving instead.

----

.. toctree::
   :maxdepth: 1
   :includehidden:
   :caption: Landscape of ML models

   docs/landscape/README.md
   docs/landscape/learning.md
   docs/landscape/hybrid.md
   docs/landscape/statistical.md
   docs/landscape/hitl.md

.. toctree::
   :maxdepth: 1
   :includehidden:
   :caption: MLOps structure

   docs/structure/README.md
   docs/structure/small.md
   docs/structure/big.md
   docs/structure/hybrid.md
   docs/structure/large.md

.. toctree::
   :maxdepth: 1
   :includehidden:
   :caption: Implementing roadmap

   docs/implement/README.md
   docs/implement/dev.md
   docs/implement/transition.md
   docs/implement/ops.md

.. toctree::
   :maxdepth: 1
   :includehidden:
   :caption: Factors for success

   docs/factors/README.md
   docs/factors/data.md
   docs/factors/reqs.md

